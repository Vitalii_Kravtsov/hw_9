package model.contact;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public enum ContactType {

    PHONE("Phone"), EMAIL("E-Mail");
    
    private final String name;

    @Override
    public String toString() {
        return this.name;
    }

}

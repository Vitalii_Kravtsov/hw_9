package model.service.network.mapper;

import java.util.List;
import java.util.stream.Collectors;


public interface Mapper<DTO, MODEL> {

    DTO toDto(MODEL model);
    MODEL toModel(DTO dto);

    default List<DTO> toDto(List<MODEL> models) {
        return models.stream().map(this::toDto).collect(Collectors.toList());
    }

    default List<MODEL> toModel(List<DTO> dtos) {
        return dtos.stream().map(this::toModel).collect(Collectors.toList());
    }

}

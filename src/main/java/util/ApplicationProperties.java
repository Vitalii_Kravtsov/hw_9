package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class ApplicationProperties {

    private static final Properties PROPERTIES = new Properties();

    static {
        loadProperties();
    }

    private ApplicationProperties() {}

    private static void loadProperties() {

        try(InputStream stream = ApplicationProperties.class.getClassLoader().getResourceAsStream("application.properties")) {
            PROPERTIES.load(stream);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

    }

    public static String getProperty(String key) {
        return PROPERTIES.getProperty(key);
    }

}

package model.contact;

import org.junit.Test;

import static org.junit.Assert.*;


public class TestContactType {

    @Test
    public void testToString() {

        assertEquals("Phone", ContactType.PHONE.toString());
        assertEquals("E-Mail", ContactType.EMAIL.toString());

    }

}

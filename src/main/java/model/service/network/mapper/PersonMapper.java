package model.service.network.mapper;

import model.Person;
import model.service.network.dto.ContactDto;


public class PersonMapper implements Mapper<ContactDto, Person> {

    private final ContactMapper contactMapper = new ContactMapper();

    @Override
    public ContactDto toDto(Person person) {

        ContactDto dto = new ContactDto();

        dto.setName(person.getFirstName() + " " + person.getLastName());
        dto.setRecords(this.contactMapper.toDto(person.getContacts()));

        return dto;

    }

    @Override
    public Person toModel(ContactDto contactDto) {

        String[] personData = contactDto.getName().split(" ");

        Person person = new Person(personData[0], personData.length > 1 ? personData[1] : "");

        person.setContacts(this.contactMapper.toModel(contactDto.getRecords()));

        return person;

    }

}

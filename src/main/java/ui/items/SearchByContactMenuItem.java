package ui.items;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.service.PhoneBookService;
import ui.Display;

import java.util.List;
import java.util.Scanner;


@RequiredArgsConstructor
public class SearchByContactMenuItem implements MenuItem {

    private final Scanner scanner;
    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Поиск по началу контакта";
    }

    @Override
    public void run() {

        System.out.print("Введите начало контакта: ");

        List<Person> persons = this.phoneBookService.searchByContact(this.scanner.next());

        if (persons != null)
            this.display.displayPersons(persons);
        else System.out.println("\nОшибка чтения контактов\n");

    }

}

package model.service;

import model.Person;
import model.contact.Contact;
import model.contact.ContactType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class MemoryPhoneBook implements PhoneBookService {

    private final List<Person> persons = new ArrayList<>();

    @Override
    public boolean add(Person person) {

        this.persons.add(person);
        
        return true;

    }

    @Override
    public List<Person> sort() {
        return this.persons.stream().sorted().collect(Collectors.toList());
    }

    @Override
    public List<Person> filter(ContactType type) {

        List<Person> filtered = new ArrayList<>();

        for (Person person : this.persons) {

            List<Contact> contacts = new ArrayList<>();

            for (Contact contact : person.getContacts())
                if (contact.getType().equals(type))
                    contacts.add(contact);

            if (contacts.size() != 0) {

                Person newPerson = new Person(person.getFirstName(), person.getLastName());

                newPerson.setContacts(contacts);

                filtered.add(newPerson);

            }

        }

        return filtered;

    }

    @Override
    public List<Person> searchByName(String pattern) {
        return this.persons.stream().filter(
                person -> person.getFirstName().toLowerCase().contains(pattern.toLowerCase())).collect(Collectors.toList()
        );
    }

    @Override
    public List<Person> searchByContact(String pattern) {

        List<Person> result = new ArrayList<>();

        for (Person person : this.persons) {

            for (Contact contact : person.getContacts())
                if (contact.getValue().startsWith(pattern)) {

                    result.add(person);

                    break;

                }

        }

        return result;

    }

    @Override
    public boolean delete(String value) {

        Person toDelete = null;

        for (Person person : this.persons)
            for (Contact contact : person.getContacts())
                if (contact.getValue().equals(value)) {

                    toDelete = person;
                    break;

                }

        if (toDelete != null) {

            this.persons.remove(toDelete);

            return true;

        } else return false;

    }

    @Override
    public boolean exists(Contact contact) {

        for (Person person : this.persons)
            for (Contact c : person.getContacts())
                if (c.equals(contact))
                    return true;

        return false;

    }

}

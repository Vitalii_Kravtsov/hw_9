package ui.items;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.service.PhoneBookService;
import ui.Display;

import java.util.List;
import java.util.Scanner;


@RequiredArgsConstructor
public class SearchByNameMenuItem implements MenuItem {

    private final Scanner scanner;
    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Поиск по имени (части имени)";
    }

    @Override
    public void run() {

        System.out.print("Введите имя (часть имени): ");

        List<Person> persons = this.phoneBookService.searchByName(this.scanner.next());

        if (persons != null)
            this.display.displayPersons(persons);
        else System.out.println("\nОшибка чтения контактов\n");

    }

}

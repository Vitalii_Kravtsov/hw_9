import com.fasterxml.jackson.databind.ObjectMapper;
import model.service.*;
import model.service.network.mapper.PersonMapper;
import ui.Display;
import ui.Menu;
import ui.ServiceSelectionMenu;
import ui.items.*;
import util.ApplicationProperties;

import java.net.http.HttpClient;
import java.util.Scanner;


public class Main {

    private static final Scanner scanner = new Scanner(System.in);

    private static final Display display = new Display();

    private static final PhoneBookService[] services = {
            new MemoryPhoneBook(),
            new CSVPhoneBook(ApplicationProperties.getProperty("file.csv")),
            new ObjectPhoneBook(ApplicationProperties.getProperty("file.object")),
            new NetworkProxyPhoneBook(
                    HttpClient.newBuilder().build(), new PersonMapper(), new ObjectMapper(),
                    ApplicationProperties.getProperty("network.login"), ApplicationProperties.getProperty("network.password")
            )
    };

    private static final PhoneBookService phoneBook = new ServiceSelectionMenu(services, scanner).run();

    private static final MenuItem[] items = {
            new AddPersonMenuItem(scanner, display, phoneBook),
            new DisplayPhoneBookMenuItem(display, phoneBook),
            new DisplayPhonesMenuItem(display, phoneBook),
            new DisplayEmailsMenuItem(display, phoneBook),
            new SearchByNameMenuItem(scanner, display, phoneBook),
            new SearchByContactMenuItem(scanner, display, phoneBook),
            new DeleteContactMenuItem(scanner, phoneBook),
            new ExitMenuItem()
    };

    private static final Menu menu = new Menu(items, scanner);

    public static void main(String[] args) {
        menu.run();
    }

}

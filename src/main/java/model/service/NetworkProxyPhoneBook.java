package model.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;
import model.service.network.dto.ContactDto;
import model.service.network.mapper.Mapper;
import util.ApplicationProperties;

import java.net.http.HttpClient;
import java.util.List;


public class NetworkProxyPhoneBook implements PhoneBookService {

    private static final int ATTEMPTS = Integer.parseInt(ApplicationProperties.getProperty("network.attempts"));

    private final NetworkPhoneBook phoneBook;

    public NetworkProxyPhoneBook(
            HttpClient client, Mapper<ContactDto, Person> mapper, ObjectMapper objectMapper, String login, String password
    ) {
        this.phoneBook = new NetworkPhoneBook(client, mapper, objectMapper, login, password);
    }

    @Override
    public boolean add(Person person) {

        boolean success = false;

        int attempts = 0;

        while (!success && attempts < ATTEMPTS) {
            success = this.phoneBook.add(person);
            attempts++;
        }

        return success;

    }

    @Override
    public List<Person> sort() {

        List<Person> result = null;

        int attempts = 0;

        while (result == null && attempts < ATTEMPTS) {
            result = this.phoneBook.sort();
            attempts++;
        }

        return result;

    }

    @Override
    public List<Person> filter(ContactType type) {

        List<Person> result = null;

        int attempts = 0;

        while (result == null && attempts < ATTEMPTS) {
            result = this.phoneBook.filter(type);
            attempts++;
        }

        return result;

    }

    @Override
    public List<Person> searchByName(String pattern) {

        List<Person> result = null;

        int attempts = 0;

        while (result == null && attempts < ATTEMPTS) {
            result = this.phoneBook.searchByName(pattern);
            attempts++;
        }

        return result;

    }

    @Override
    public List<Person> searchByContact(String pattern) {

        List<Person> result = null;

        int attempts = 0;

        while (result == null && attempts < ATTEMPTS) {
            result = this.phoneBook.searchByContact(pattern);
            attempts++;
        }

        return result;

    }

    @Override
    public boolean delete(String value) {
        return false;
    }

    @Override
    public boolean exists(Contact contact) {
        return this.phoneBook.exists(contact);
    }

}

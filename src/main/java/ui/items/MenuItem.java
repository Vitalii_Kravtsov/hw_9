package ui.items;


public interface MenuItem {

    boolean isFinal();

    String getMessage();

    void run();

}

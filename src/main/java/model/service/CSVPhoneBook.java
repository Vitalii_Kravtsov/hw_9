package model.service;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
public class CSVPhoneBook implements PhoneBookService {

    private final String filename;

    @Override
    public boolean add(Person person) {

        try(PrintWriter writer = new PrintWriter(new FileWriter(this.filename, true))) {

            String fullName = person.getFirstName() + "," + person.getLastName();

            for (Contact contact : person.getContacts())
                writer.println(fullName + "," + contact.getType() + "," + contact.getValue());

            writer.flush();

            return true;

        } catch (IOException exception) {
            return false;
        }

    }

    @Override
    public List<Person> sort() {

        List<Person> persons = this.readFromCsv();

        if (persons == null)
            return null;

        return persons.stream().sorted().collect(Collectors.toList());

    }

    @Override
    public List<Person> filter(ContactType type) {

        List<Person> persons = this.readFromCsv();

        if (persons == null)
            return null;

        List<Person> filtered = new ArrayList<>();

        for (Person person : persons) {

            List<Contact> contacts = new ArrayList<>();

            for (Contact contact : person.getContacts())
                if (contact.getType().equals(type))
                    contacts.add(contact);

            if (contacts.size() != 0) {

                Person newPerson = new Person(person.getFirstName(), person.getLastName());

                newPerson.setContacts(contacts);

                filtered.add(newPerson);

            }

        }

        return filtered;

    }

    @Override
    public List<Person> searchByName(String pattern) {

        List<Person> persons = this.readFromCsv();

        if (persons == null)
            return null;

        return persons.stream().filter(
                person -> person.getFirstName().toLowerCase().contains(pattern.toLowerCase())).collect(Collectors.toList()
        );

    }

    @Override
    public List<Person> searchByContact(String pattern) {

        List<Person> persons = this.readFromCsv();

        if (persons == null)
            return null;

        List<Person> result = new ArrayList<>();

        for (Person person : persons) {

            for (Contact contact : person.getContacts())
                if (contact.getValue().startsWith(pattern)) {

                    result.add(person);

                    break;

                }

        }

        return result;

    }

    @Override
    public boolean delete(String value) {

        List<String[]> splittedLines;

        try(BufferedReader reader = new BufferedReader(new FileReader(this.filename))) {
            splittedLines = reader.lines().map(line -> line.split(",")).collect(Collectors.toList());
        } catch (IOException exception) {
            return false;
        }

        String firstName = null, lastName = null;

        for (String[] splittedLine : splittedLines)
            if (splittedLine[splittedLine.length-1].equals(value)) {
                firstName = splittedLine[0];
                lastName = splittedLine[1];
            }

        if (firstName == null)
            return false;

        try(PrintWriter writer = new PrintWriter(new FileWriter(this.filename))) {

            String finalFirstName = firstName;
            String finalLastName = lastName;

            splittedLines.stream().filter(
                    splittedLine -> !splittedLine[0].equals(finalFirstName) || !splittedLine[1].equals(finalLastName)
            ).map(
                    splittedLine -> String.join(",", splittedLine)
            ).forEach(writer::println);

            writer.flush();

            return true;

        } catch (IOException exception) {
            return false;
        }

    }

    @Override
    public boolean exists(Contact contact) {

        List<Person> persons = this.readFromCsv();

        if (persons == null)
            return false;

        for (Person person : persons)
            for (Contact c : person.getContacts())
                if (c.equals(contact))
                    return true;

        return false;

    }

    private List<Person> readFromCsv() {

        try(BufferedReader reader = new BufferedReader(new FileReader(this.filename))) {

            List<Person> persons = new ArrayList<>();

            String firstName = null;
            String lastName = null;

            Person person = null;

            List<Contact> contacts = new ArrayList<>();

            for (String[] splittedLine : reader.lines().map(line -> line.split(",")).collect(Collectors.toList())) {

                if (!splittedLine[0].equals(firstName) || !splittedLine[1].equals(lastName)) {

                    if (person != null) {
                        person.setContacts(contacts);
                        contacts = new ArrayList<>();
                        persons.add(person);
                    }

                    firstName = splittedLine[0];
                    lastName = splittedLine[1];

                    person = new Person(firstName, lastName);

                }

                ContactType[] values = ContactType.values();

                ContactType type = null;

                for (ContactType ct : values)
                    if (ct.toString().equals(splittedLine[2]))
                        type = ct;

                contacts.add(new Contact(type, splittedLine[3]));

            }

            if (person != null) {
                person.setContacts(contacts);
                persons.add(person);
            }

            return persons;

        } catch (IOException exception) {
            return null;
        }

    }

}

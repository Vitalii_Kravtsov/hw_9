package ui.items;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.service.PhoneBookService;
import model.contact.ContactType;
import ui.Display;

import java.util.List;


@RequiredArgsConstructor
public class DisplayPhonesMenuItem implements MenuItem {

    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Посмотреть только телефоны";
    }

    @Override
    public void run() {

        List<Person> persons = this.phoneBookService.filter(ContactType.PHONE);

        if (persons != null)
            this.display.displayPersons(persons);
        else System.out.println("\nОшибка чтения контактов\n");

    }

}

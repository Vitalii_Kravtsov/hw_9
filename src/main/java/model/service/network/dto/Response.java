package model.service.network.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    String status;
    List<ContactDto> contacts;

    String token;

    String error;

    public boolean isOk() {
        return this.status != null && "OK".equalsIgnoreCase(this.status);
    }

}

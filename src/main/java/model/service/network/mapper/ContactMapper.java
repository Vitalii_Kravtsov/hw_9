package model.service.network.mapper;

import model.contact.Contact;
import model.contact.ContactType;
import model.service.network.dto.RecordDto;


public class ContactMapper implements Mapper<RecordDto, Contact> {

    @Override
    public RecordDto toDto(Contact contact) {

        RecordDto dto = new RecordDto();

        dto.setType(contact.getType().name().toLowerCase());
        dto.setValue(contact.getValue());

        return dto;

    }

    @Override
    public Contact toModel(RecordDto recordDto) {
        return new Contact(ContactType.valueOf(recordDto.getType().toUpperCase()), recordDto.getValue());
    }

}

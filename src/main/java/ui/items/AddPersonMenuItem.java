package ui.items;

import lombok.RequiredArgsConstructor;
import model.Person;
import model.service.PhoneBookService;
import model.contact.Contact;
import model.contact.ContactType;
import ui.Display;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;


@RequiredArgsConstructor
public class AddPersonMenuItem implements MenuItem {

    private final Scanner scanner;
    private final Display display;
    private final PhoneBookService phoneBookService;

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public String getMessage() {
        return "Добавить контакт";
    }

    @Override
    public void run() {

        System.out.print("Введите имя: ");
        String firstName = this.scanner.next();

        System.out.print("Введите фамилию: ");
        String lastName = this.scanner.next();

        Person person = new Person(firstName, lastName);

        List<Contact> contacts = new ArrayList<>();

        while (true) {

            try {

                this.display.displayContactTypeChoice();

                System.out.print("Тип контакта: ");

                int typeIndex = scanner.nextInt() - 1;

                if (typeIndex == -1) break;

                    try {

                        System.out.print("\nЗначение контакта: ");

                        Contact contact = new Contact(ContactType.values()[typeIndex], scanner.next());

                        if (!(this.phoneBookService.exists(contact) || contacts.contains(contact))) contacts.add(contact);
                        else System.out.println("\nКонтакт уже существует\n");

                    } catch (IndexOutOfBoundsException ignored) {
                        System.out.println("\nНеправильный тип контакта\n");
                    }

            } catch (InputMismatchException exception) {

                scanner.nextLine();

                System.out.println("\nНеправильный тип контакта\n");

            }

        }

        person.setContacts(contacts);

        if (!this.phoneBookService.add(person))
            System.out.println("\nОшибка добавления контакта\n");

    }

}
